#!/bin/bash
#echo "The script for creating users"
#read -p "You need to specify account: " username
#echo "Creating account with username: $username"
#(grep "^$username:" /etc/passwd > /dev/null && echo "User $username already exists") || (useradd -m -s /bin/bash $username && echo "User $username was created")

echo "The script for creating users" #Вывод текста на экран
read -p "You need to specify file with users list: " file
read -p "Which action do you need? Choose: add/delete: " action
for line in $(cat $file)
do
	username=$(echo $line | cut -d : -f 1)
        if [[ $action == "add" ]]
        then
                group=$(echo $line | cut -d : -f 2)
                password=$(openssl passwd -1 $(echo $line | cut -d : -f 3))
                echo "Creating account with username: $username"
                if [[ $(grep "^$username:" /etc/passwd) ]]
                then
                        echo "User $username already exists"
                        exit
                else
                        useradd -m -s /bin/bash $username
                        echo "User $username was created"
                fi
                if [[ ! $(grep "^$group:" /etc/group) ]] # Если НЕ (!) находит
                then
                        echo "Group $group doesn't exist"
                        groupadd $group
                        echo "Group $group was created!"
                fi
                usermod -aG $group -p $password $username
                echo -e "User $username was added to group $group\n"
        elif [[ $action == "delete" ]]
        then
                if [[ $(grep "^$username:" /etc/passwd) ]]
                then
                        userdel -rf $username
                        echo -e "$username was deleted!\n"
                fi
        else
                echo "Option $action doesn't exist. Choose add/delete!"
                exit
        fi
done


#read -p "Enter a number: " number


#if [[ number -gt 4 ]]  # если число больше 4
#then # тогда
	#echo "Number greater than 4" # некий блок выполняющийся при условии
#elif [[ number -lt 4 ]] # less than, меньше чем 4
#then 
	#echo "Number less than 4"
#else
	#echo "Number = 4"
#fi

#echo "The number is: $number"
